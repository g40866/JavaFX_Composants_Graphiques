package esi.atl.component.rdvb;

import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

public class Led extends Region {

    private static final double   BORDER_RSIZE = 0.02;
    private double size;

    private final BooleanProperty on;
    private final ObjectProperty<Color> color;           
    private final Canvas canvas;
    private final GraphicsContext context;

    public Led() {
	on = new SimpleBooleanProperty(true);
	color = new ObjectPropertyBase<Color>() {
                @Override
                public Object getBean() {
                    return this;
                }
                @Override
                public String getName() {
                    return "Color";
                }
            };
        canvas = new Canvas();
        context = canvas.getGraphicsContext2D();
        context.setStroke(Color.BLACK);
        getChildren().add(canvas);
        setColor(Color.RED);
        addListeners();
    }
    // Add InvalidationListener to width and height properties
    private void addListeners() {
        widthProperty().addListener(o -> resizeCanvas());
        heightProperty().addListener(o -> resizeCanvas());
    }
    public final void setOn(boolean on) {
        this.on.set(on);
        repaint();
    }
    public final boolean isOn() {
        return on.get();
    }
    public final BooleanProperty onProperty() {
        return on;
    }
    public final Color getColor() {
        return color.get();
    }
    public final void setColor(Color color) {
        this.color.set(color);
        repaint();
    }
    public final ObjectProperty<Color> colorProperty() {
        return color;
    }
    private void resizeCanvas() {
        double width = getWidth();
        double height = getHeight();

        // resizing the canvas to the biggest possible square in the parent (region)
        size = width < height ? width : height;
        canvas.setWidth(size);
        canvas.setHeight(size);

        // centering the canvas in the parent (region)
        if (width > height) {
            canvas.relocate((width - size)*0.5, 0);
        } else if (height > width) {
            canvas.relocate(0, (height - size)*0.5);
        }
        repaint();
    }
    
    private void repaint() {
        double ulc = BORDER_RSIZE * size;       // canvas's upper left corner
        double brc = (1-2*BORDER_RSIZE) * size; // canvas's bottom right corner
        
        context.clearRect(0, 0, size, size);        // clean the canvas        
        context.strokeOval(ulc, ulc, brc, brc);     // draw the led's border

        if (isOn()) {       // fill the led with the color property value if is is on
            context.setFill(color.get());    
            context.fillOval(ulc, ulc, brc, brc);
        }
    }
}