package esi.atl.component.pf;

import javafx.scene.control.Button;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

/**
 *
 * @author G40866
 */
public class MainLed extends Application {

    private BorderPane root;
    private Scene scene;
    private HBox hbox;
    
    private Button on_off;
    private Led led;
    private ColorPicker colorPicker;
    
    private Label onOffTxt;
    
    
    
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        
        primaryStage.setTitle("Led");
        
        root = new BorderPane();
        
        led = new Led();
        
        buttonOnOff();
        
        root.setCenter(led);
        
        colorPicker();
        
        observableValue();
              
        onOffColorPicker();
        
        scene = new Scene(root, 400, 200);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
    
    
    public void buttonOnOff(){
        
        on_off = new Button("On/Off");
        on_off.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (led.isOn()) {
                    led.setOn(false);
                } else {
                    led.setOn(true);
                }
            }
        });
    }
    
    public void colorPicker(){
        
        colorPicker = new ColorPicker();
        colorPicker.setOnAction(new EventHandler() {

            @Override
            public void handle(Event t) {
                led.setColor(colorPicker.getValue());
            }
        });
    }
    
    public void onOffColorPicker(){
        
        hbox = new HBox(10);
        hbox.getChildren().addAll(on_off, colorPicker, onOffTxt);
        //hbox.getChildren().addAll(on_off, colorPicker);
        hbox.setPadding(new Insets(15, 12, 15, 12));
        root.setBottom(hbox);
    }
    
    public void observableValue(){
    
    onOffTxt = new Label();

    led.onProperty ().addListener(
        (ObservableValue<? extends Boolean> observable,
                Boolean oldValue, Boolean newValue) -> {
            onOffTxt.setText("avant clic : " + oldValue + ", après : " + newValue);
    });
    }
}
