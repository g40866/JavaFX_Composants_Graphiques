package esi.atl.component.rvlb;

import javafx.beans.binding.Bindings;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.scene.shape.Circle;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ObjectPropertyBase;
import javafx.scene.layout.Region;
import javafx.scene.paint.Color;

public class Led extends Region {

    private final BooleanProperty on;
    private final ObjectProperty<Color> color;           
    private final Circle circle;

    public Led() {
	on = new SimpleBooleanProperty(true);
	color = new ObjectPropertyBase<Color>() {
                @Override
                public Object getBean() {
                    return this;
                }
                @Override
                public String getName() {
                    return "Color";
                }
            };
        circle = new Circle(50);
        circle.setStroke(Color.BLACK);
        getChildren().add(circle);
        setColor(Color.RED);
        doBindings();
    }
    private void doBindings() {
        circle.centerXProperty().bind(widthProperty().divide(2));
        circle.centerYProperty().bind(heightProperty().divide(2));
        circle.radiusProperty().bind(
                Bindings.subtract(Bindings.min(widthProperty().divide(2),                
                                               heightProperty().divide(2)),
                                  10));        
        circle.fillProperty().bind(new FillShapeBinding(color, on));
    }
    public final void setOn(boolean on) {
        this.on.set(on);
    }
    public final boolean isOn() {
        return on.get();
    }
    public final BooleanProperty onProperty() {
        return on;
    }
    public final Color getColor() {
        return color.get();
    }
    public final void setColor(Color color) {
        this.color.set(color);
    }
    public final ObjectProperty<Color> colorProperty() {
        return color;
    }
}
